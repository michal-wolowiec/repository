package builder;

import domain.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface IEntityBuilder<TEntity extends Entity> {
    TEntity build(ResultSet rs) throws SQLException;
}
