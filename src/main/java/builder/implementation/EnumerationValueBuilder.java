package builder.implementation;

import builder.IEntityBuilder;
import domain.EnumerationValue;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EnumerationValueBuilder implements IEntityBuilder<EnumerationValue> {
    public EnumerationValue build(ResultSet rs) throws SQLException {
        EnumerationValue enumerationValue = new EnumerationValue();

        enumerationValue.setKey(rs.getString("key"));
        enumerationValue.setName(rs.getString("name"));
        enumerationValue.setValue(rs.getString("value"));

        return enumerationValue;
    }
}
