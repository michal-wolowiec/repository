package builder.implementation;

import builder.IEntityBuilder;
import domain.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserBuilder implements IEntityBuilder<User> {
    public User build(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getString("id"));
        user.setLogin(rs.getString("login"));
        user.setPassword(rs.getString("password"));
        return user;
    }
}
