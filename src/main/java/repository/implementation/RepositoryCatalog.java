package repository.implementation;

import builder.implementation.EnumerationValueBuilder;
import builder.implementation.PersonBuilder;
import builder.implementation.UserBuilder;
import repository.IEnumerationValueRepository;
import repository.IRepositoryCatalog;
import repository.IUserRepository;
import unitofwork.implementation.UnitOfWork;

import java.sql.Connection;

public class RepositoryCatalog implements IRepositoryCatalog {
    private Connection connection;
    private UnitOfWork unitOfWork;

    public RepositoryCatalog(Connection connection, UnitOfWork unitOfWork) {
        this.connection = connection;
        this.unitOfWork = unitOfWork;
    }

    public IUserRepository getUsers() {
        return new UserRepository(this.connection, new UserBuilder(), this.unitOfWork);
    }

    public PersonRepository getPersons() {
        return new PersonRepository(this.connection, new PersonBuilder(), this.unitOfWork);
    }

    public IEnumerationValueRepository getEnumerationValues() {
        return new EnumerationValueRepository(this.connection, new EnumerationValueBuilder(), this.unitOfWork);
    }

    public void commit() {
        this.unitOfWork.commit();
    }
}
