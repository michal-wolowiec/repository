package repository.implementation;

import builder.IEntityBuilder;
import domain.Entity;
import repository.IRepository;
import rulechecker.CheckResult;
import rulechecker.RuleChecker;
import rulechecker.RuleResult;
import unitofwork.IUnitOfWorkRepository;
import unitofwork.implementation.UnitOfWork;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class Repository<TEntity extends Entity>
        implements IRepository<TEntity>, IUnitOfWorkRepository<TEntity> {
    protected UnitOfWork unitOfWork;
    protected Connection connection;
    protected IEntityBuilder<TEntity> entityBuilder;
    protected RuleChecker<TEntity> ruleChecker;

    protected PreparedStatement selectById;
    protected PreparedStatement selectAll;
    protected PreparedStatement insert;
    protected PreparedStatement delete;
    protected PreparedStatement update;
    protected PreparedStatement count;

    protected String sqlTemplateCount =
            "SELECT COUNT(*) AS total FROM " + this.getTableName();
    protected String sqlTemplateSelectAll =
            "SELECT * FROM " + this.getTableName();
    protected String sqlTemplateSelectById =
            this.sqlTemplateSelectAll + " WHERE id=?";
    protected String sqlTemplateDelete =
            "DELETE FROM " + this.getTableName() + " WHERE id=?";

    protected Repository(Connection connection, IEntityBuilder<TEntity> entityBuilder, UnitOfWork unitOfWork) {
        this.connection = connection;
        this.entityBuilder = entityBuilder;
        this.unitOfWork = unitOfWork;
        this.ruleChecker = new RuleChecker<TEntity>();

        try {
            this.selectById = connection.prepareStatement(this.sqlTemplateSelectById);
            this.selectAll = connection.prepareStatement(this.sqlTemplateSelectAll);
            this.delete = connection.prepareStatement(this.sqlTemplateDelete);
            this.count = connection.prepareStatement(this.sqlTemplateCount);
            this.insert = connection.prepareStatement(getSqlTemplateForInsert());
            this.update = connection.prepareStatement(getSqlTemplateForUpdate());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void persistInsert(TEntity entity) {
        try {
            prepareQueryForInsert(entity);

            if (!this.validData(entity)) {
                return;
            }

            this.insert.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void persistUpdate(TEntity entity) {
        try {
            prepareQueryForUpdate(entity);

            if (!this.validData(entity)) {
                return;
            }

            this.update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void persistDelete(TEntity entity) {
        try {
            prepareQueryForDelete(entity);
            this.delete.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Boolean validData(TEntity entity) {
        for (CheckResult result : this.ruleChecker.check(entity)) {
            if (result.getResult() == RuleResult.Error) {
                System.out.println("Error raised by " + result.getMessage());
                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    public void update(TEntity entity) {
        this.unitOfWork.markAsModified(entity, this);
    }

    public void delete(TEntity entity) {
        this.unitOfWork.markAsDeleted(entity, this);
    }

    public void save(TEntity entity) {
        this.unitOfWork.markAsNew(entity, this);
    }

    public Integer count() {
        Integer resultCount = 0;

        try {
            ResultSet resultSet = this.count.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("total");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public TEntity getById(String id) {
        try {
            this.selectById.setString(1, id);
            ResultSet resultSet = this.selectById.executeQuery();
            if (resultSet.next()) {
                return this.entityBuilder.build(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public List<TEntity> getAll() {
        List<TEntity> resultList = new ArrayList<TEntity>();

        try {
            ResultSet resultSet = this.selectAll.executeQuery();
            while (resultSet.next()) {
                resultList.add(this.entityBuilder.build(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultList;
    }

    protected void prepareQueryForDelete(TEntity entity) throws SQLException {
        this.delete.setString(1, entity.getId());
    }

    protected abstract void prepareQueryForUpdate(TEntity entity) throws SQLException;

    protected abstract void prepareQueryForInsert(TEntity entity) throws SQLException;

    protected abstract String getTableName();

    protected abstract String getSqlTemplateForInsert();

    protected abstract String getSqlTemplateForUpdate();
}
