package repository.implementation;

import builder.IEntityBuilder;
import domain.EnumerationValue;
import repository.IEnumerationValueRepository;
import unitofwork.implementation.UnitOfWork;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EnumerationValueRepository extends Repository<EnumerationValue> implements IEnumerationValueRepository {
    protected PreparedStatement selectByKey;
    protected PreparedStatement selectByName;

    protected String sqlTemplateSelectByKey =
            "SELECT * FROM " + this.getTableName() + " WHERE key=?";
    protected String sqlTemplateSelectByName =
            "SELECT * FROM " + this.getTableName() + " WHERE name=?";

    protected EnumerationValueRepository(Connection connection, IEntityBuilder<EnumerationValue> entityBuilder, UnitOfWork unitOfWork) {
        super(connection, entityBuilder, unitOfWork);

        try {
            this.selectByKey = connection.prepareStatement(this.sqlTemplateSelectByKey);
            this.selectByName = connection.prepareStatement(this.sqlTemplateSelectByName);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public EnumerationValue getByKey(String key) {
        try {
            this.selectByKey.setString(1, key);
            ResultSet resultSet = this.selectByKey.executeQuery();
            if (resultSet.next()) {
                return this.entityBuilder.build(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public EnumerationValue getByName(String name) {
        try {
            this.selectByName.setString(1, name);
            ResultSet resultSet = this.selectByName.executeQuery();
            if (resultSet.next()) {
                return this.entityBuilder.build(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void prepareQueryForUpdate(EnumerationValue entity) throws SQLException {
        this.update.setString(1, entity.getKey());
        this.update.setString(2, entity.getName());
        this.update.setString(3, entity.getValue());
        this.update.setString(4, entity.getId());
    }

    @Override
    protected void prepareQueryForInsert(EnumerationValue entity) throws SQLException {
        this.insert.setString(1, entity.getKey());
        this.insert.setString(2, entity.getName());
        this.insert.setString(3, entity.getValue());
    }

    @Override
    protected String getTableName() {
        return "t_sys_enums";
    }

    @Override
    protected String getSqlTemplateForInsert() {
        return "INSERT INTO " + this.getTableName() + "(key, name, value) VALUES(?, ?, ?)";
    }

    @Override
    protected String getSqlTemplateForUpdate() {
        return "UPDATE " + this.getTableName() + " SET (key, name, value)=(?, ?, ?) WHERE id=?";
    }
}
