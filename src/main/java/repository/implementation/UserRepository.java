package repository.implementation;

import builder.IEntityBuilder;
import domain.User;
import repository.IUserRepository;
import rulechecker.rules.LoginRule;
import rulechecker.rules.PasswordRule;
import unitofwork.implementation.UnitOfWork;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRepository extends Repository<User> implements IUserRepository {
    protected PreparedStatement selectByLogin;
    protected PreparedStatement selectByLoginAndPassword;

    protected String sqlTemplateSelectByLogin =
            "SELECT * FROM " + this.getTableName() + " WHERE login=?";
    protected String sqlTemplateSelectByLoginAndPassword =
            this.sqlTemplateSelectByLogin + " AND password=?";

    public UserRepository(Connection connection, IEntityBuilder<User> entityBuilder, UnitOfWork unitOfWork) {
        super(connection, entityBuilder, unitOfWork);

        try {
            this.selectByLogin = connection.prepareStatement(this.sqlTemplateSelectByLogin);
            this.selectByLoginAndPassword = connection.prepareStatement(this.sqlTemplateSelectByLoginAndPassword);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.ruleChecker.addRule(new LoginRule());
        this.ruleChecker.addRule(new PasswordRule());
    }

    public User getByLoginAndPassword(String login, String password) {
        try {
            this.selectByLoginAndPassword.setString(1, login);
            this.selectByLoginAndPassword.setString(2, password);
            ResultSet resultSet = this.selectByLoginAndPassword.executeQuery();
            if (resultSet.next()) {
                return this.entityBuilder.build(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public User getByLogin(String login) {
        try {
            this.selectByLogin.setString(1, login);
            ResultSet resultSet = this.selectByLogin.executeQuery();
            if (resultSet.next()) {
                return this.entityBuilder.build(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void setupPermissions(User user) {

    }

    public void setupRoles(User user) {

    }

    @Override
    protected void prepareQueryForUpdate(User entity) throws SQLException {
        this.update.setString(1, entity.getLogin());
        this.update.setString(2, entity.getPassword());
        this.update.setString(3, entity.getId());
    }

    @Override
    protected void prepareQueryForInsert(User entity) throws SQLException {
        this.insert.setString(1, entity.getLogin());
        this.insert.setString(2, entity.getPassword());
    }

    @Override
    protected String getTableName() {
        return "t_sys_users";
    }

    @Override
    protected String getSqlTemplateForInsert() {
        return "INSERT INTO " + this.getTableName() + "(login, password) VALUES(?, ?)";
    }

    @Override
    protected String getSqlTemplateForUpdate() {
        return "UPDATE " + this.getTableName() + " SET (login, password)=(?, ?) WHERE id=?";
    }
}
