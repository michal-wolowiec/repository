package repository.implementation;

import builder.IEntityBuilder;
import domain.Person;
import repository.IPersonRepository;
import rulechecker.rules.NipRule;
import rulechecker.rules.PeselRule;
import unitofwork.implementation.UnitOfWork;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;

public class PersonRepository extends Repository<Person> implements IPersonRepository {
    protected PreparedStatement selectByFirstName;
    protected PreparedStatement selectByLastName;

    protected String sqlTemplateSelectByFirstName =
            "SELECT * FROM " + this.getTableName() + " WHERE first_name=?";
    protected String sqlTemplateSelectByLastName =
            "SELECT * FROM " + this.getTableName() + " WHERE last_name=?";

    protected PersonRepository(Connection connection, IEntityBuilder<Person> entityBuilder, UnitOfWork unitOfWork) {
        super(connection, entityBuilder, unitOfWork);

        try {
            this.selectByFirstName = connection.prepareStatement(this.sqlTemplateSelectByFirstName);
            this.selectByLastName = connection.prepareStatement(this.sqlTemplateSelectByLastName);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.ruleChecker.addRule(new NipRule());
        this.ruleChecker.addRule(new PeselRule());
    }

    public Person getByFirstName(String firstName) {
        try {
            this.selectByFirstName.setString(1, firstName);
            ResultSet resultSet = this.selectByFirstName.executeQuery();
            if (resultSet.next()) {
                return this.entityBuilder.build(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Person getByLastName(String lastName) {
        try {
            this.selectByFirstName.setString(1, lastName);
            ResultSet resultSet = this.selectByLastName.executeQuery();
            if (resultSet.next()) {
                return this.entityBuilder.build(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void prepareQueryForUpdate(Person entity) throws SQLException {
        this.update.setString(1, entity.getFirstName());
        this.update.setString(2, entity.getLastName());
        this.update.setString(3, entity.getPesel());
        this.update.setString(4, entity.getNip());
        this.update.setString(5, entity.getEmail());
        this.update.setString(6, DateFormat.getDateInstance().format(entity.getDateOfBirth()));
        this.update.setString(7, entity.getId());
    }

    @Override
    protected void prepareQueryForInsert(Person entity) throws SQLException {
        this.insert.setString(1, entity.getFirstName());
        this.insert.setString(2, entity.getLastName());
        this.insert.setString(3, entity.getPesel());
        this.insert.setString(4, entity.getNip());
        this.insert.setString(5, entity.getEmail());
        this.insert.setString(6, DateFormat.getDateInstance().format(entity.getDateOfBirth()));
    }

    @Override
    protected String getTableName() {
        return "t_p_people";
    }

    @Override
    protected String getSqlTemplateForInsert() {
        return "INSERT INTO " + this.getTableName() + "(first_name, last_name, pesel, nip, email, date_of_birth) " +
                "VALUES(?, ?, ?, ?, ?, ?)";
    }

    @Override
    protected String getSqlTemplateForUpdate() {
        return "UPDATE " + this.getTableName() + " SET (first_name, last_name, pesel, nip, email, date_of_birth)=" +
                "(?, ?, ?, ?, ?, ?) WHERE id=?";
    }
}
