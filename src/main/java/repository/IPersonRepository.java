package repository;

import domain.Person;

public interface IPersonRepository extends IRepository<Person> {
    Person getByFirstName(String firstName);

    Person getByLastName(String lastName);
}
