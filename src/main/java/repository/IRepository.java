package repository;

import java.util.List;

public interface IRepository<TEntity> {
    void update(TEntity entity);

    void delete(TEntity entity);

    void save(TEntity entity);

    Boolean validData(TEntity entity);

    Integer count();

    TEntity getById(String id);

    List<TEntity> getAll();
}
