package repository;

import domain.EnumerationValue;

public interface IEnumerationValueRepository extends IRepository<EnumerationValue> {
    EnumerationValue getByKey(String key);

    EnumerationValue getByName(String name);
}
