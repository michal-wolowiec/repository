package repository;

public interface IRepositoryCatalog {
    IUserRepository getUsers();

    IPersonRepository getPersons();

    IEnumerationValueRepository getEnumerationValues();

    void commit();
}
