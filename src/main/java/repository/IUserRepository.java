package repository;

import domain.User;

public interface IUserRepository extends IRepository<User> {
    User getByLoginAndPassword(String login, String password);

    User getByLogin(String login);

    void setupPermissions(User user);

    void setupRoles(User user);
}
