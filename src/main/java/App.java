import domain.User;
import repository.implementation.RepositoryCatalog;
import unitofwork.implementation.UnitOfWork;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class App {
    public static void main(String[] args) {
        String databaseUrl = "jdbc:hsqldb:hsql://localhost/workdb";

        User test = new User();
        test.setLogin("test");
        test.setPassword("test123");

        try {
            Class.forName("org.hsqldb.jdbcDriver");
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        try {
            Connection connection = DriverManager.getConnection(databaseUrl);
            UnitOfWork unitOfWork = new UnitOfWork(connection);
            RepositoryCatalog repositoryCatalog = new RepositoryCatalog(connection, unitOfWork);

            repositoryCatalog.getUsers().save(test);
            unitOfWork.commit();

            List<User> usersFromDatabase = repositoryCatalog.getUsers().getAll();
            for (User user : usersFromDatabase) {
                System.out.println(user.getId() + " " + user.getLogin() + " " + user.getPassword());
            }

            System.out.println(repositoryCatalog.getUsers().count());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
