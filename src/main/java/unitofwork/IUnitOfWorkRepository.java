package unitofwork;

public interface IUnitOfWorkRepository<TEntity> {
    void persistInsert(TEntity entity);

    void persistUpdate(TEntity entity);

    void persistDelete(TEntity entity);
}
