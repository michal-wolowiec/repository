package unitofwork.implementation;

import domain.Entity;
import domain.EntityState;
import unitofwork.IUnitOfWork;
import unitofwork.IUnitOfWorkRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

public class UnitOfWork implements IUnitOfWork {
    private Connection connection;
    private Map<Entity, IUnitOfWorkRepository<Entity>> entities;

    public UnitOfWork(Connection connection) {
        this.connection = connection;
        this.entities = new LinkedHashMap<Entity, IUnitOfWorkRepository<Entity>>();

        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void commit() {
        for (Entity entity : entities.keySet()) {
            switch (entity.getState()) {
                case New:
                    entities.get(entity).persistInsert(entity);
                    break;

                case Modified:
                    entities.get(entity).persistUpdate(entity);
                    break;

                case Deleted:
                    entities.get(entity).persistDelete(entity);
                    break;
            }
        }

        try {
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        this.rollback();
    }

    public void rollback() {
        entities.clear();
    }

    public void markAsNew(Entity entity, IUnitOfWorkRepository repository) {
        entity.setState(EntityState.New);
        entities.put(entity, repository);
    }

    public void markAsDeleted(Entity entity, IUnitOfWorkRepository repository) {
        entity.setState(EntityState.Deleted);
        entities.put(entity, repository);
    }

    public void markAsModified(Entity entity, IUnitOfWorkRepository repository) {
        entity.setState(EntityState.Modified);
        entities.put(entity, repository);
    }
}
