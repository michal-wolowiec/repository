package unitofwork;

import domain.Entity;

public interface IUnitOfWork {
    void commit();

    void rollback();

    void markAsNew(Entity entity, IUnitOfWorkRepository repository);

    void markAsDeleted(Entity entity, IUnitOfWorkRepository repository);

    void markAsModified(Entity entity, IUnitOfWorkRepository repository);
}
