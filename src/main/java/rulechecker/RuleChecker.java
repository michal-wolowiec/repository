package rulechecker;

import java.util.ArrayList;
import java.util.List;

public class RuleChecker<TEntity> {
    private List<Rule<TEntity>> rules;

    public RuleChecker() {
        this.rules = new ArrayList<Rule<TEntity>>();
    }

    public List<CheckResult> check(TEntity entity) {
        List<CheckResult> results = new ArrayList<CheckResult>();

        for (Rule<TEntity> rule : this.rules) {
            results.add(rule.check(entity));
        }

        return results;
    }

    public List<Rule<TEntity>> getRules() {
        return rules;
    }

    public void setRules(List<Rule<TEntity>> rules) {
        this.rules = rules;
    }

    public void addRule(Rule<TEntity> rule) {
        this.rules.add(rule);
    }


}
