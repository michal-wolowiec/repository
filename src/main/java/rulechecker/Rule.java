package rulechecker;

public interface Rule<TEntity> {
    CheckResult check(TEntity entity);
}
