package rulechecker;

public class CheckResult {
    private String message;
    private RuleResult result;

    public CheckResult(String message, RuleResult result) {
        this.message = message;
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public RuleResult getResult() {
        return result;
    }
}
