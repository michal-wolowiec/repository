package rulechecker;

public enum RuleResult {
    Ok,
    Error,
    Exception
}
