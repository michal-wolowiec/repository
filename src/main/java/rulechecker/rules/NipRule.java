package rulechecker.rules;

import domain.Person;
import rulechecker.CheckResult;
import rulechecker.Rule;
import rulechecker.RuleResult;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NipRule implements Rule<Person> {
    public CheckResult check(Person person) {
        if (person.getNip() == null) {
            return new CheckResult("Nip cannot be null.", RuleResult.Error);
        }

        if (person.getNip().length() != 10) {
            return new CheckResult("Nip must be of length 10.", RuleResult.Error);
        }

        Integer[] weights = {6, 5, 7, 2, 3, 4, 5, 6, 7};
        Integer controlSum = 0;

        String nip = person.getNip();
        Integer nipLastDigit = Integer.parseInt(nip.substring(9));

        for (int i = 0; i < weights.length; i++) {
            controlSum += Integer.parseInt(nip.substring(i, i + 1)) * weights[i];
        }

        if (controlSum % 11 != nipLastDigit) {
            return new CheckResult("Nip is not valid.", RuleResult.Error);
        }

        return new CheckResult("Nip is fine.", RuleResult.Ok);
    }
}
