package rulechecker.rules;

import domain.User;
import rulechecker.CheckResult;
import rulechecker.Rule;
import rulechecker.RuleResult;

public class PasswordRule implements Rule<User> {
    public CheckResult check(User user) {
        if (user.getPassword() == null) {
            return new CheckResult("Password cannot be null.", RuleResult.Error);
        }

        if (user.getPassword().length() < 8) {
            return new CheckResult("Password must be at least 8 characters long.", RuleResult.Error);
        }

        if (!user.getPassword().matches(".*[A-Z].*[A-Z].*")) {
            return new CheckResult("Password must have at least two uppercased character.", RuleResult.Error);
        }

        return new CheckResult("Password is fine.", RuleResult.Ok);
    }
}
