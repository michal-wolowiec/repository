package rulechecker.rules;

import domain.Person;
import rulechecker.CheckResult;
import rulechecker.Rule;
import rulechecker.RuleResult;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailRule implements Rule<Person> {
    private Pattern pattern;
    private Matcher matcher;

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

    public CheckResult check(Person person) {
        if (person.getEmail() == null) {
            return new CheckResult("Email cannot be null.", RuleResult.Error);
        }

        if (person.getEmail().isEmpty()){
            return new CheckResult("Email cannot be empty.", RuleResult.Error);
        }

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(person.getEmail());

        if (!matcher.matches()) {
            return new CheckResult("Email is invalid.", RuleResult.Error);
        }

        return new CheckResult("Email is fine.", RuleResult.Ok);
    }
}
