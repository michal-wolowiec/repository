package rulechecker.rules;

import domain.Person;
import rulechecker.CheckResult;
import rulechecker.Rule;
import rulechecker.RuleResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DateOfBirthRule implements Rule<Person> {

    Person person = new Person();

    public CheckResult check(Person person) {
        if (person.getDateOfBirth() == null) {
            return new CheckResult("Date of birth cannot be null.", RuleResult.Error);
        }

        DateFormat dateFormat = new SimpleDateFormat("yyMMdd");

        String dateString = dateFormat.format(person.getDateOfBirth());
        String datePeselString = person.getPesel().substring(0, 7);

        if (!dateString.equals(datePeselString)) {
            return new CheckResult("Date of birth is invalid.", RuleResult.Error);
        }

        return new CheckResult("Date of birth is fine.", RuleResult.Ok);
    }
}
