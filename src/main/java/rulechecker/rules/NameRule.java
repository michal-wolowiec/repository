package rulechecker.rules;

import domain.Person;
import domain.User;
import rulechecker.CheckResult;
import rulechecker.Rule;
import rulechecker.RuleResult;

public class NameRule implements Rule<Person> {
    public CheckResult check(Person person) {
        if (person.getFirstName() == null) {
            return new CheckResult("First name cannot be null.", RuleResult.Error);
        }

        if (person.getFirstName().isEmpty()) {
            return new CheckResult("First name cannot be empty.", RuleResult.Error);
        }

        return new CheckResult("First name is fine.", RuleResult.Ok);
    }
}
