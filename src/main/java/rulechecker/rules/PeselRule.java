package rulechecker.rules;

import domain.Person;
import rulechecker.CheckResult;
import rulechecker.Rule;
import rulechecker.RuleResult;

public class PeselRule implements Rule<Person> {
    public CheckResult check(Person person) {
        if (person.getPesel() == null) {
            return new CheckResult("Pesel cannot be null.", RuleResult.Error);
        }

        if (person.getPesel().length() != 11) {
            return new CheckResult("Pesel must be of length 10.", RuleResult.Error);
        }

        Integer[] weights = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};
        Integer controlSum = 0;

        String pesel = person.getPesel();
        String peselLastDigit = pesel.substring(10);

        for (int i = 0; i < weights.length; i++) {
            controlSum += Integer.parseInt(pesel.substring(i, i + 1)) * weights[i];
        }

        String controlSumLastDigit = controlSum.toString().substring(
                controlSum.toString().length() - 1);

        if (!peselLastDigit.equals(controlSumLastDigit)) {
            return new CheckResult("Pesel is not valid.", RuleResult.Error);
        }

        return new CheckResult("Pesel is fine.", RuleResult.Ok);
    }
}
