package rulechecker.rules;

import domain.User;
import rulechecker.CheckResult;
import rulechecker.Rule;
import rulechecker.RuleResult;

public class LoginRule implements Rule<User> {
    public CheckResult check(User user) {
        if (user.getLogin() == null) {
            return new CheckResult("Login cannot be null.", RuleResult.Error);
        }

        if (user.getLogin().isEmpty()) {
            return new CheckResult("Login cannot be empty.", RuleResult.Error);
        }

        return new CheckResult("Login is fine.", RuleResult.Ok);
    }
}
