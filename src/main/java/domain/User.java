package domain;

import javax.persistence.Column;
import javax.persistence.Table;
import java.util.List;

@Table(name = "T_SYS_USERS")
public class User extends Entity {
    @Column(name = "login")
    private String login;
    @Column(name = "password")
    private String password;
    private List<UserRoles> roles;
    private List<UserPermissions> permissions;

    public User() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<UserRoles> getRoles() {
        return roles;
    }

    public void setRoles(List<UserRoles> roles) {
        this.roles = roles;
    }

    public List<UserPermissions> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<UserPermissions> permissions) {
        this.permissions = permissions;
    }
}
