package domain;

import javax.persistence.*;

@javax.persistence.Entity
public abstract class Entity {
    @Id @GeneratedValue
    @Column(name = "id")
    private String id;
    private EntityState state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public EntityState getState() {
        return state;
    }

    public void setState(EntityState state) {
        this.state = state;
    }
}
