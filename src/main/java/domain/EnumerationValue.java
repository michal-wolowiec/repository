package domain;

import javax.persistence.Column;

public class EnumerationValue extends Entity {
    @Column(name = "key")
    private String key;
    @Column(name = "value")
    private String value;
    @Column(name = "name")
    private String name;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
